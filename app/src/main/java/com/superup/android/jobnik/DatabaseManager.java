package com.superup.android.jobnik;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.superup.android.jobnik.model.User;

import java.util.Random;

/**
 * Created by Andrey Rankov on 09/03/2017.
 */

public class DatabaseManager {

    private static DatabaseReference reference;

    private DatabaseManager() {
    }

    public static DatabaseReference getReference() {
        if (reference == null) {
            reference = FirebaseDatabase.getInstance().getReference();
        }
        return reference;
    }

    public static void addDemoUser(String userId) {
        User user = new User();
        user.setFirstName("Vlad");
        user.setLastName("Karpman");
        user.setCost(50);
        user.setRating(5);
        user.setUserPic("http://letzstepout.com/images/default-userpic.jpg");
        user.setLatitude(32.069315);
        user.setLongitude(34.791212);
        user.setProfession("Electrician");

        reference.child("users").child(userId).setValue(user);
    }

    public static void addDemoUsers(int count) {
        int i = 0;
        Random random = new Random();

        String[] names = {"Vlad", "Ido", "Andrey", "Fil", "Roy", "Adam", "Arik", "Nik", "Steve", "Bill", "Elon", "Aaron", "Tomer", "Itay", "Levi", "Rafi", "David"};
        String[] lastNames = {"Geller", "Abraham", "Sokolow", "Ginzberg", "Kovalsky", "Rawitz", "Sorkin", "Zahav", "Zalman", "Bragin", "Cohen", "Baruch", "Bar-Yehuda", "Davidson"};
        double[] costs = {15, 20, 25, 30, 35, 40, 45, 50};
        double[] ratings = {1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5};
        String[] professions = {"Electrician", "Plumber", "Chef", "Detective", "Mobile", "Teacher", "Courier", "Showman", "Writer", "Loader", "Cleaner", "Driver"};
        double latitude = 32.066772;
        double longitude = 34.825974;
        String profession;

        while (i < count) {
            User user = new User();
            user.setId(i);
            user.setFirstName(randomString(names, random));
            user.setLastName(randomString(lastNames, random));
            user.setCost(randomNumber(costs, random));
            user.setRating(randomNumber(ratings, random));
            user.setLatitude(randomCoordinate(latitude, random));
            user.setLongitude(randomCoordinate(longitude, random));
            profession = randomString(professions, random);
            user.setProfession(profession);
            user.setUserPic(chooseUserPic(profession));
            i++;

            reference.child("users").child(String.valueOf(i)).setValue(user);
        }
    }

    private static String chooseUserPic(String profession) {
        switch (profession){
            case "Electrician":
                return "https://firebasestorage.googleapis.com/v0/b/jobnik-e7e5a.appspot.com/o/userIcons%2Fworker.png?alt=media&token=7237fd0f-91a3-404c-9d14-c18770195798";
            case "Plumber":
                return "https://firebasestorage.googleapis.com/v0/b/jobnik-e7e5a.appspot.com/o/userIcons%2Fworker-1.png?alt=media&token=fd892823-4e77-40ef-92a5-6238648bbec8";
            case "Chef":
                return "https://firebasestorage.googleapis.com/v0/b/jobnik-e7e5a.appspot.com/o/userIcons%2Fchef.png?alt=media&token=b98d6b7c-75f7-43e0-80bb-f4eab79a5cd6";
            case "Detective":
                return "https://firebasestorage.googleapis.com/v0/b/jobnik-e7e5a.appspot.com/o/userIcons%2Fdetective.png?alt=media&token=23dc26b5-1147-4a08-824c-0a9b539eef65";
            case "Mobile":
                return "https://firebasestorage.googleapis.com/v0/b/jobnik-e7e5a.appspot.com/o/userIcons%2Fengineer.png?alt=media&token=4fdb8f19-303f-4e0b-8250-7e872cce7976";
            case "Teacher":
                return "https://firebasestorage.googleapis.com/v0/b/jobnik-e7e5a.appspot.com/o/userIcons%2Fteacher.png?alt=media&token=df296670-8a7b-4830-9c4e-7ea82524c61b";
            case "Courier":
                return "https://firebasestorage.googleapis.com/v0/b/jobnik-e7e5a.appspot.com/o/userIcons%2Fcourier.png?alt=media&token=e355f8d4-6faa-48b5-a315-61b827fa7022";
            case "Showman":
                return "https://firebasestorage.googleapis.com/v0/b/jobnik-e7e5a.appspot.com/o/userIcons%2Fshowman.png?alt=media&token=b723c28f-d9fb-4026-b7dc-bfff72cfcf1e";
            case "Writer":
                return "https://firebasestorage.googleapis.com/v0/b/jobnik-e7e5a.appspot.com/o/userIcons%2Fjournalist.png?alt=media&token=eb0b2397-c8e0-4e90-8c54-5e3cfc9f22b0";
            case "Loader":
                return "https://firebasestorage.googleapis.com/v0/b/jobnik-e7e5a.appspot.com/o/userIcons%2Floader.png?alt=media&token=4abfa3c1-42e9-476e-b2e6-2ab649ba6c53";
            case "Cleaner":
                return "https://firebasestorage.googleapis.com/v0/b/jobnik-e7e5a.appspot.com/o/userIcons%2Fsoldier.png?alt=media&token=ae90db58-7336-4e7e-a991-2d16470868bf";
            case "Driver":
                return "https://firebasestorage.googleapis.com/v0/b/jobnik-e7e5a.appspot.com/o/userIcons%2Ftaxi-driver.png?alt=media&token=6381559b-a5ee-47e1-8985-652445e46c74";
            default:
                return "https://firebasestorage.googleapis.com/v0/b/jobnik-e7e5a.appspot.com/o/userIcons%2FdefaultAvatar.png?alt=media&token=8c5443c0-f0ab-4eab-bdb4-3f3361b7f779";
        }
    }

    private static double randomCoordinate(double coordinate, Random random) {
        return coordinate + random.nextInt(20) * 0.01;
    }

    private static double randomNumber(double[] numbers, Random random) {
        return numbers[random.nextInt(numbers.length)];
    }


    private static String randomString(String[] strings, Random random) {
        return strings[random.nextInt(strings.length)];
    }


    public static void readUsers() {
        reference.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    d.getValue(User.class);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void getLocations(double latitude, double longitude, double radius) {
        GeoFire geoFire = new GeoFire(reference);
        GeoQuery geoQuery = geoFire.queryAtLocation(new GeoLocation(latitude, longitude), radius);
        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                System.out.println(String.format("Key %s entered the search area at [%f,%f]", key, location.latitude, location.longitude));
            }

            @Override
            public void onKeyExited(String key) {
                System.out.println(String.format("Key %s is no longer in the search area", key));
            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {
                System.out.println(String.format("Key %s moved within the search area to [%f,%f]", key, location.latitude, location.longitude));
            }

            @Override
            public void onGeoQueryReady() {
                System.out.println("All initial data has been loaded and events have been fired!");
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {
                System.err.println("There was an error with this query: " + error);
            }
        });
    }

}
