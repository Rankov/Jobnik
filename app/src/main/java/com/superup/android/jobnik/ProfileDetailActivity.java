package com.superup.android.jobnik;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.BaseMultiplePermissionsListener;

import java.util.List;
import java.util.Random;

import jp.wasabeef.glide.transformations.CropCircleTransformation;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by vladislavkarpman on 3/10/17.
 */
public class ProfileDetailActivity extends AppCompatActivity {

    private static final float WINDOW_PERCENT_HEIGHT = 0.80f;
    private static final float WINDOW_PERCENT_WIDTH = 0.90f;

    private ImageView userPic;
    private TextView name;
    private TextView cost;
    private TextView profession;
    private TextView distance;
    private MaterialRatingBar rating;
    private TextView ratesNumber;
    private TextView call;
    private TextView message;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_edit_activity);

        DisplayMetrics metrics = getResources().getDisplayMetrics();

        int screenHeight = (int) (metrics.heightPixels * WINDOW_PERCENT_HEIGHT);
        int screenWidth = (int) (metrics.widthPixels * WINDOW_PERCENT_WIDTH);

        setFinishOnTouchOutside(true);
        getWindow().setLayout(screenWidth, screenHeight);
        getWindow().setGravity(Gravity.CENTER);

        initViews();
        setData();
    }

    private void initViews() {
        userPic = (ImageView) findViewById(R.id.profile_image);
        name = (TextView) findViewById(R.id.name);
        cost = (TextView) findViewById(R.id.hour_rate_number);
        profession = (TextView) findViewById(R.id.skill);
        distance = (TextView) findViewById(R.id.distance_number);
        rating = (MaterialRatingBar) findViewById(R.id.rating);
        ratesNumber = (TextView) findViewById(R.id.rating_number);
        call = (TextView) findViewById(R.id.call);
        message = (TextView) findViewById(R.id.message);
    }

    private void setData() {
        Bundle bundle = getIntent().getExtras();

        Glide.with(getApplicationContext())
                .load(bundle.getString("userPic"))
                .bitmapTransform(new CropCircleTransformation(getApplicationContext()))
                .into(userPic);

        name.setText(String.format("%s %s", bundle.getString("name"), bundle.getString("lastName")));
        cost.setText(String.format("%d ₪", (int) bundle.getDouble("cost")));
        profession.setText(bundle.getString("profession"));
        distance.setText(String.format("%d km", (int) bundle.getDouble("distance")));
        rating.setRating((float) bundle.getDouble("rating"));
        ratesNumber.setText(String.valueOf(new Random().nextInt(100)) + " votes");

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dexter.withActivity(ProfileDetailActivity.this)
                        .withPermissions(Manifest.permission.CALL_PHONE)
                        .withListener(new BaseMultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                super.onPermissionsChecked(report);
                                if (!report.areAllPermissionsGranted()) return;
                                Intent intent = new Intent(Intent.ACTION_CALL);
                                intent.setData(Uri.parse("tel:12125551212"));
                                startActivity(intent);
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                super.onPermissionRationaleShouldBeShown(permissions, token);
                                token.continuePermissionRequest();
                            }
                        })
                        .check();
            }
        });

        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:"));
                sendIntent.putExtra("address", "12125551212");
                startActivity(sendIntent);
            }
        });


    }
}
