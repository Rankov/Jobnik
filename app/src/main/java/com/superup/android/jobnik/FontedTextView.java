package com.superup.android.jobnik;

/**
 * Created by vladislavkarpman on 3/10/17.
 */

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;


/**
 * Created by vladislavkarpman on 11/30/16.
 */
public class FontedTextView extends android.support.v7.widget.AppCompatTextView {

    private static Typeface defaultTypeface;

    public FontedTextView(Context context) {
        super(context);
        setDefaultTextFont(context);
    }

    public FontedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setDefaultTextFont(context);
    }

    public FontedTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setDefaultTextFont(context);
    }

    private void setDefaultTextFont(Context ctx) {
        if (defaultTypeface == null) {

            TypedValue typedValue = new TypedValue();
            ctx.getApplicationContext().getTheme().resolveAttribute(R.attr.DefaultApplicationTextFont, typedValue, false);

            if (typedValue.coerceToString() == null) return;

            String font = typedValue.coerceToString().toString();

            defaultTypeface = Typeface.createFromAsset(ctx.getAssets(), font);
        }

        setTypeface(defaultTypeface);
    }
}
