package com.superup.android.jobnik;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.superup.android.jobnik.model.User;

import java.util.List;
import java.util.Vector;

import jp.wasabeef.glide.transformations.CropCircleTransformation;
import rouchuan.customlayoutmanager.CustomLayoutManager;

/**
 * Created by ido_klzujtc on 09/03/2017.
 */

public class ProfilesAdapter extends RecyclerView.Adapter {
    private RecyclerView recyclerView;
    private List<User> userList = new Vector<>();

    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onClick(RecyclerView recyclerView,View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public ProfilesAdapter() {
        setHasStableIds(true);
    }


    public List<User> getUserList() {
        return userList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProfileViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.detail_view_holder, parent, false));
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
        this.notifyDataSetChanged();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        this.recyclerView = recyclerView;
    }

    private boolean isGridEnabled = false;
    @Override
    public long getItemId(int position) {
        return userList.get(position).getId();
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        User user = userList.get(position);
        ProfileViewHolder profileViewHolder = (ProfileViewHolder) holder;

        Glide.with(profileViewHolder.itemView.getContext())
                .load(user.getUserPic())
                .bitmapTransform(new CropCircleTransformation(holder.itemView.getContext()))
                .into(profileViewHolder.profileImage);

        profileViewHolder.lastName.setText(user.getLastName());
        profileViewHolder.firstName.setText(user.getFirstName());
        profileViewHolder.hourlyPay.setText(String.format("%d ₪", (int) user.getCost()));
        profileViewHolder.jobType.setText(user.getProfession());
        profileViewHolder.ratingBar.setRating((float) user.getRating());
        profileViewHolder.distance.setText(String.format("%.1f km",user.getDistanse()));
        profileViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(recyclerView.getLayoutManager() instanceof CustomLayoutManager) {
                    RecyclerView.ViewHolder centerView = recyclerView.findContainingViewHolder(recyclerView.findChildViewUnder(recyclerView.getMeasuredWidth() / 2, recyclerView.getMeasuredHeight() / 2));

                    if (centerView != null) {
                        if (centerView == holder) {
                            if (onItemClickListener != null) {
                                onItemClickListener.onClick(recyclerView,v, holder.getAdapterPosition());
                            }
                        } else {
                            smoothScrollToPoint(recyclerView, holder, recyclerView.getMeasuredWidth() / 2, recyclerView.getMeasuredHeight() / 2, holder.itemView.getMeasuredWidth() / 2, holder.itemView.getMeasuredHeight() / 2);
                        }
                    }
                } else {
                    if (onItemClickListener != null)
                        onItemClickListener.onClick(recyclerView,v, holder.getAdapterPosition());
                }
            }
        });
//        profileViewHolder.distance.setText(user.ge);

        if (!isGridEnabled) {
            holder.itemView.getLayoutParams().width = (int) (recyclerView.getMeasuredWidth() / 2.5f);
        } else {
            holder.itemView.getLayoutParams().width = (int) (recyclerView.getMeasuredWidth() / 2.3f);
            holder.itemView.getLayoutParams().height = (int) (recyclerView.getMeasuredHeight() / 2.5f);
        }

        //profileViewHolder.jobType.setText(user.ge);
    }

    public void isSnapEnable(boolean isSnapEnable) {

    }

    public void setGridEnabled(boolean gridEnabled) {
        isGridEnabled = gridEnabled;
    }

    /**
     * smooth scroll the recyclerView viewHolder to given point
     *
     * @param recyclerView that will be scroll
     * @param holder       to scroll to (middle point of the holder
     * @param X            OF THE POINT RELATIVE TO RECYCLER VIEW
     * @param Y            OF THE POINT RELATIVE TO RECYCLER VIEW
     */
    public static void smoothScrollToPoint(RecyclerView recyclerView, RecyclerView.ViewHolder holder, int X, int Y,int offestX,int offestY) {
        if (holder != null) {
            float dX = X - (holder.itemView.getX() + offestX);
            float dY = Y - (holder.itemView.getY() + offestY);

            if(Math.abs(dX) > 2f || Math.abs(dY) > 2f)
                recyclerView.smoothScrollBy((int) -dX, (int) -dY);
        }
    }



    @Override
    public int getItemCount() {
        return userList.size();
    }

    private class ProfileViewHolder extends RecyclerView.ViewHolder {

        private RatingBar ratingBar;
        private ImageView profileImage;
        private TextView firstName;
        private TextView lastName;
        private TextView hourlyPay;
        private TextView jobType;
        private TextView distance;

        public ProfileViewHolder(View itemView) {
            super(itemView);

            profileImage = (ImageView) itemView.findViewById(R.id.profile_image);
            firstName = (TextView) itemView.findViewById(R.id.first_name);
            lastName = (TextView) itemView.findViewById(R.id.last_name);
            hourlyPay = (TextView) itemView.findViewById(R.id.hourly_pay);
            jobType = (TextView) itemView.findViewById(R.id.job_type);
            distance = (TextView) itemView.findViewById(R.id.location_distance);
            ratingBar = (RatingBar) itemView.findViewById(R.id.rating);
        }
    }
}
