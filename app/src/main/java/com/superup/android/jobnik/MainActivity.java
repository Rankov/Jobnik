package com.superup.android.jobnik;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.BaseMultiplePermissionsListener;
import com.superup.android.jobnik.model.User;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import static com.superup.android.jobnik.R.id.map;


public class MainActivity extends AppCompatActivity {

    private MapFragment mapFragment;
    private FloatingSearchView searchBar;
    private View mapView;

    private GoogleMap googleMap;

    private ImageView plusButton;

    private OnMapReadyCallback mapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            MainActivity.this.googleMap = googleMap;
            fetchUsers();
            askPermissionAndRunTask(new Runnable() {
                @Override
                public void run() {
                    fetchCurrentLocation();
                }
            });

            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    if (markersToUsers.containsKey(marker)) {
                        User user = markersToUsers.get(marker);
                        int index = profilesAdapter.getUserList().indexOf(user);

                        if (index >= 0) {
                            recyclerView.scrollToPosition(index);
                            setUserOnMap(user);
                        }
                    }
                    return false;
                }
            });
        }
    };
    private RecyclerView recyclerView;
    private RecyclerView gridRecyclerView;
    private ScrollZoomLayoutManager scrollZoomLayoutManager;
    private GridLayoutManager gridLayoutManager;
    private ProfilesAdapter profilesAdapter = new ProfilesAdapter();
    private ProfilesAdapter profilesGridAdapter = new ProfilesAdapter();


    private GoogleApiClient googleApiClient;
    private Location lastCurrentLocation;
    private LatLng position;
    private List<User> users = new Vector<>();

    private PagerSnapHelper pagerSnapHelper;
    private TabLayout tabLayout;

    private ProfilesAdapter.OnItemClickListener onItemClickListener = new ProfilesAdapter.OnItemClickListener() {
        @SuppressLint("NewApi")
        @Override
        public void onClick(RecyclerView recyclerView,View view, int position) {
            RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(position);

            if(viewHolder == null)
                return;


            ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this,
                    viewHolder.itemView.findViewById(R.id.profile_image),
                    "profile");

            Intent intent = new Intent(MainActivity.this, ProfileDetailActivity.class);
            User user = profilesAdapter.getUserList().get(position);
            intent.putExtra("userPic", user.getUserPic());
            intent.putExtra("name", user.getFirstName());
            intent.putExtra("lastName", user.getLastName());
            intent.putExtra("cost", user.getCost());
            intent.putExtra("profession", user.getProfession());
            intent.putExtra("distance", user.getDistanse());
            intent.putExtra("rating", user.getRating());


            startActivity(intent, activityOptions.toBundle());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        activeIcon = getBitmapDescriptor(R.drawable.ic_location_active);
        notActiveIcon = getBitmapDescriptor(R.drawable.ic_location_not_active);
        currentLocationIcon = getBitmapDescriptor(R.drawable.ic_location_current);

        // Create an instance of GoogleAPIClient.
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .build();

        // find views
        findViews();

        mapFragment.getMapAsync(mapReadyCallback);

        searchBar.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onActionMenuItemSelected(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_location:
                        askPermissionAndRunTask(new Runnable() {
                            @Override
                            public void run() {
                                fetchCurrentLocation();
                            }
                        });
                        break;
                }
            }
        });

        initRecyclerView();

        searchBar.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, String newQuery) {
                invalidateSearch(users, newQuery, false);
            }
        });

        searchBar.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {
                invalidateSearch(users, searchSuggestion.getBody(), true);
            }

            @Override
            public void onSearchAction(String currentQuery) {
                invalidateSearch(users, currentQuery, true);
            }
        });

        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_map));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_list));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setTabIconColor(tab, R.color.blue);
                if (tab.getPosition() == 0) {
                    if (recyclerView.getVisibility() != View.VISIBLE) {
                        gridRecyclerView.animate().translationY(gridRecyclerView.getMeasuredHeight() * 1.7f).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                recyclerView.setVisibility(View.VISIBLE);
                                searchBar.animate().translationY(0);
                                recyclerView.animate().translationY(0);
                                gridRecyclerView.setVisibility(View.GONE);
                            }
                        });
                    }
                } else if (tab.getPosition() == 1) {
                    /*if (profilesGridAdapter.getUserList() == null || profilesGridAdapter.getUserList().isEmpty()) {
                        profilesGridAdapter.setUserList(profilesAdapter.getUserList());
                    }*/
                    searchBar.animate().translationY(- searchBar.getMeasuredHeight() * 2);
                    gridRecyclerView.setVisibility(View.VISIBLE);
                    recyclerView.animate().translationY(recyclerView.getMeasuredHeight() * 1.5f).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            recyclerView.setVisibility(View.GONE);
                            gridRecyclerView.animate().translationY(0);
                        }
                    });
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                setTabIconColor(tab, R.color.black);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                setTabIconColor(tab, R.color.blue);
            }
        });
        tabLayout.getTabAt(0).select();

        profilesAdapter.setOnItemClickListener(onItemClickListener);
        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private Timer timer = new Timer();

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 101 && resultCode == -1) {
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                            ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    lastCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    if (lastCurrentLocation != null) {

                        final double latitude = lastCurrentLocation.getLatitude();
                        final double longitude = lastCurrentLocation.getLongitude();

                        position = new LatLng(latitude, longitude);

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                moveCameraWithZoom(position);
                            }
                        });
                        cancel();
                    }
                }
            }, 0, 30);
        }
    }

    private void setTabIconColor(TabLayout.Tab tab, int color) {
        switch (tab.getPosition()) {
            case 0:
                DrawableCompat.setTint(tabLayout.getTabAt(0).getIcon(), ContextCompat.getColor(getApplicationContext(), color));
                break;
            case 1:
                DrawableCompat.setTint(tabLayout.getTabAt(1).getIcon(), ContextCompat.getColor(getApplicationContext(), color));
                break;
        }
    }

    private void initRecyclerView() {
        scrollZoomLayoutManager = new ScrollZoomLayoutManager(getApplication(), Dp2px(10));
        recyclerView = (RecyclerView) findViewById(R.id.profile_recycler);
        recyclerView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(scrollZoomLayoutManager);
        recyclerView.setAdapter(profilesAdapter);
        pagerSnapHelper = new PagerSnapHelper();
        pagerSnapHelper.attachToRecyclerView(recyclerView);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            int prevState = RecyclerView.SCROLL_STATE_IDLE;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                updateProfilesPositions();
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (prevState == newState) return;
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    RecyclerView.ViewHolder viewHolder = getCenterViewHolder(recyclerView);
                    if (viewHolder != null && viewHolder.getAdapterPosition() >= 0) {
                        User user = profilesAdapter.getUserList().get(viewHolder.getAdapterPosition());
                        setUserOnMap(user);
                    }
                }
                prevState = newState;

            }
        });

        gridLayoutManager = new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false);
        profilesGridAdapter.setGridEnabled(true);
        profilesGridAdapter.setOnItemClickListener(onItemClickListener);
        gridRecyclerView.addItemDecoration(new SpacesItemDecoration((int) (getResources().getDisplayMetrics().widthPixels * 0.04f)));
        gridRecyclerView.setAdapter(profilesGridAdapter);
        gridRecyclerView.setTranslationY(gridRecyclerView.getMeasuredHeight() * 1.1f);
        gridRecyclerView.setLayoutManager(gridLayoutManager);
    }

    private class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, android.support.v7.widget.RecyclerView parent, android.support.v7.widget.RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);

            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top = space;
        }
    }

    private Marker activeMarker;
    private BitmapDescriptor activeIcon;
    private BitmapDescriptor notActiveIcon;
    private BitmapDescriptor currentLocationIcon;

    private void setUserOnMap(User user) {
        moveCameraWithZoom(new LatLng(user.getLatitude(), user.getLongitude()));
        Marker current = usersToMarkers.get(user);

        if (activeMarker != current) {
            if (activeMarker != null) {
                activeMarker.setIcon(notActiveIcon);
            }
            activeMarker = current;
            activeMarker.setIcon(activeIcon);
        }
    }

    private BitmapDescriptor getBitmapDescriptor(int id) {
        Drawable vectorDrawable = getDrawable(id);
        int h = dpToPx(42);
        int w = dpToPx(42);
        vectorDrawable.setBounds(0, 0, w, h);
        Bitmap bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_4444);
        Canvas canvas = new Canvas(bm);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bm);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    @SuppressWarnings("MissingPermission")
    private void fetchCurrentLocation() {
        try {
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            boolean isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            boolean isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                turnOnCurrentLocation(this, googleApiClient);
            } else {
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER, 0, 0, new android.location.LocationListener() {
                                @Override
                                public void onLocationChanged(Location location) {
                                    if (lastCurrentLocation != null) {
                                        return;
                                    }
                                    lastCurrentLocation = location;
                                    moveCameraToCurrentLocation(new LatLng(lastCurrentLocation.getLatitude(), lastCurrentLocation.getLongitude()));
                                }

                                @Override
                                public void onStatusChanged(String s, int i, Bundle bundle) {
                                }

                                @Override
                                public void onProviderEnabled(String s) {
                                }

                                @Override
                                public void onProviderDisabled(String s) {
                                }
                            });

                    lastCurrentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (lastCurrentLocation != null) {
                        moveCameraToCurrentLocation(new LatLng(lastCurrentLocation.getLatitude(), lastCurrentLocation.getLongitude()));
                    }
                }
                if (isGPSEnabled) {
                    if (lastCurrentLocation == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER, 0, 0, new android.location.LocationListener() {
                                    @Override
                                    public void onLocationChanged(Location location) {
                                        if (lastCurrentLocation != null) {
                                            return;
                                        }
                                        lastCurrentLocation = location;
                                        moveCameraToCurrentLocation(new LatLng(lastCurrentLocation.getLatitude(), lastCurrentLocation.getLongitude()));
                                    }

                                    @Override
                                    public void onStatusChanged(String s, int i, Bundle bundle) {
                                    }

                                    @Override
                                    public void onProviderEnabled(String s) {
                                    }

                                    @Override
                                    public void onProviderDisabled(String s) {
                                    }
                                });

                        lastCurrentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (lastCurrentLocation != null) {
                            moveCameraToCurrentLocation(new LatLng(lastCurrentLocation.getLatitude(), lastCurrentLocation.getLongitude()));
                            if (lastCurrentLocation != null) {
                                moveCameraWithZoom(new LatLng(lastCurrentLocation.getLatitude(), lastCurrentLocation.getLongitude()));
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void askPermissionAndRunTask(final Runnable runnable) {
        Dexter.withActivity(MainActivity.this)
                .withPermissions(android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new BaseMultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        super.onPermissionsChecked(report);
                        if (!report.areAllPermissionsGranted()) return;
                        runnable.run();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        super.onPermissionRationaleShouldBeShown(permissions, token);
                        token.continuePermissionRequest();
                    }
                })
                .check();
    }

    /**
     * Turning on current location service
     *
     * @param activity        Activity object
     * @param googleApiClient Google Api Client
     */
    public static void turnOnCurrentLocation(final Activity activity, GoogleApiClient googleApiClient) {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true); //this is the key ingredient

        LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
                .setResultCallback(new ResultCallback<LocationSettingsResult>() {
                    @Override
                    public void onResult(LocationSettingsResult result) {
                        final Status status = result.getStatus();
                        final LocationSettingsStates state = result.getLocationSettingsStates();
                        switch (status.getStatusCode()) {
                            case LocationSettingsStatusCodes.SUCCESS:
                                // All location settings are satisfied. The client can initialize location
                                // requests here.
                                break;
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                // Location settings are not satisfied. But could be fixed by showing the user
                                // a dialog.
                                try {
                                    // Show the dialog by calling startResolutionForResult(),
                                    // and check the result in onActivityResult().
                                    status.startResolutionForResult(activity, 101);
                                } catch (IntentSender.SendIntentException ignored) {
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                // Location settings are not satisfied. However, we have no way to fix the
                                // settings so we won't show the dialog.
                                break;
                        }
                    }
                });
    }

    private void updateProfilesPositions() {
        int childCount = recyclerView.getChildCount();

        if (childCount == 0) return;

        View first = recyclerView.getChildAt(0);

        float tittleWidth = first.getMeasuredWidth();

        // offset from center point plus scrolled X
        float centerOffset = recyclerView.getScrollX();

        for (int i = 0; i < childCount; i++) {

            View profile = recyclerView.getChildAt(i);

            // just to be on a safe side
            if (profile == null) continue;

            // X coordinate of center of certain bubble
            float titleCardCenterX = profile.getX() + tittleWidth / 2f - recyclerView.getResources().getDisplayMetrics().widthPixels / 2f;

            // distance from center of bubble to center point
            float distanceFromMiddle = Math.abs(centerOffset - titleCardCenterX);

            float deltaHeight = (float) (0.0001f * Math.pow(distanceFromMiddle, 2f));

            profile.setRotation((float) Math.toDegrees(Math.atan(2f * 0.0001f * distanceFromMiddle * Math.signum(titleCardCenterX - centerOffset))));
            profile.setTranslationY(deltaHeight);
        }
    }


    public static RecyclerView.ViewHolder getCenterViewHolder(RecyclerView recyclerView) {
        try {
            View view = recyclerView.findChildViewUnder(recyclerView.getMeasuredWidth() / 2, recyclerView.getMeasuredHeight() / 2);

            if (view != null)
                return recyclerView.findContainingViewHolder(view);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static final int limit = 7;

    @SuppressWarnings("MissingPermission")
    public void invalidateSearch(List<User> users, String searchQuery, boolean fullSearch) {
        invalidateSearch(users, searchQuery, lastCurrentLocation, fullSearch);
    }

    public void invalidateSearch(List<User> users, Location lastLocation) {
        invalidateSearch(users, searchBar.getQuery(), lastLocation, false);
    }

    @SuppressWarnings("MissingPermission")
    public void invalidateSearch(List<User> users) {
        invalidateSearch(users, searchBar.getQuery(), lastCurrentLocation, false);
    }


    public void invalidateSearch(List<User> users, String searchQuery, Location lastLocation, boolean fullSearch) {
        List<SearchSuggestion> searchSuggestions = new Vector<>();
        boolean isNotNullOrEmpty = searchQuery != null && !searchQuery.isEmpty();
        List<User> relevantUsers = users;

        if (isNotNullOrEmpty)
            relevantUsers = getRelevantUsers(relevantUsers, searchQuery);

        if (lastLocation != null)
            relevantUsers = sortUsersByDistance(relevantUsers,
                    lastLocation.getLatitude(),
                    lastLocation.getLongitude());

        if (isNotNullOrEmpty) {
            for (String profession : getProfessions(relevantUsers)) {
                searchSuggestions.add(new StringSearchSuggestion(profession));

                if (searchSuggestions.size() > limit)
                    break;
            }
        }

        filterMarkers(fullSearch, isNotNullOrEmpty, relevantUsers);


        searchBar.swapSuggestions(searchSuggestions);
        profilesAdapter.setUserList(relevantUsers);
        profilesGridAdapter.setUserList(profilesAdapter.getUserList());
    }

    private void filterMarkers(boolean fullSearch, boolean isNotNullOrEmpty, List<User> relevantUsers) {
        if (fullSearch) {
            Set<User> userSet = new HashSet<>(relevantUsers);

            for (Map.Entry<User, Marker> marker : usersToMarkers.entrySet()) {
                boolean wasVisible = marker.getValue().isVisible();

                if (wasVisible != userSet.contains(marker.getKey()))
                    marker.getValue().setVisible(!wasVisible);
            }
        } else if (!isNotNullOrEmpty) {
            for (Map.Entry<User, Marker> marker : usersToMarkers.entrySet()) {
                if (!marker.getValue().isVisible())
                    marker.getValue().setVisible(true);
            }
        }
    }

    private double computeDistance(User user, double lat, double lon) {
        return distance(user.getLatitude(), user.getLongitude(), lat, lon);
    }

    /**
     * Calculate distance between two points in latitude and longitude taking
     * into account height difference. If you are not interested in height
     * difference pass 0.0. Uses Haversine method as its base.
     * <p>
     * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters
     * el2 End altitude in meters
     *
     * @returns Distance in Meters
     */
    public static double distance(double lat1, double lon1,
                                  double lat2, double lon2) {

        final int R = 6371; // Radius of the earth

        Double latDistance = Math.toRadians(lat2 - lat1);
        Double lonDistance = Math.toRadians(lon2 - lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c; // convert to meters
        return distance;
    }

    public Set<String> getProfessions(List<User> users) {
        Set<String> professions = new HashSet<>();

        for (User user : users) {
            professions.add(user.getProfession());
        }

        return professions;
    }

    public List<User> getRelevantUsers(List<User> users, String searchStr) {
        List<User> newUsers = new Vector<>();
        searchStr = searchStr.toLowerCase();
        for (User user : users) {
            if (user.getProfession().toLowerCase().contains(searchStr) || searchStr.contains(user.getProfession().toLowerCase()))
                newUsers.add(user);
        }

        return newUsers;
    }

    public List<User> sortUsersByDistance(List<User> users, final double lat, final double lon) {
        List<User> newUsers = new Vector<>(users);

        for (User user : newUsers) {
            user.setDistanse(computeDistance(user, lat, lon));
        }

        Collections.sort(newUsers, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return Double.compare(o1.getDistanse(), o2.getDistanse());
            }
        });

        return newUsers;
    }


    private boolean isCameraRunning = false;

    public void clearMarkers() {
        googleMap.clear();
    }

    private Marker currentLocationMarker; //= new MarkerOptions().icon(activeIcon).title("currentLocation");

    public void moveCameraToCurrentLocation(final LatLng position) {
        if (googleMap != null) {
            if (currentLocationMarker == null)
                currentLocationMarker = googleMap.addMarker(new MarkerOptions().icon(currentLocationIcon).position(position).visible(true).title("current location"));
            else
                currentLocationMarker.setPosition(position);
        }

        moveCameraWithZoom(position);
    }

    private void moveCameraWithZoom(final LatLng position) {
        if (timer != null) {
            timer.cancel();
        }
        if (lastCurrentLocation != null && !isCameraRunning) {
            isCameraRunning = true;

            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 13), new GoogleMap.CancelableCallback() {
                @Override
                public void onFinish() {
                    isCameraRunning = false;
                }

                @Override
                public void onCancel() {
                    isCameraRunning = false;
                }
            });
        }
    }

    private int Dp2px(float dp) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    private void fetchUsers() {
        DatabaseManager.getReference().child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    users.add(snapshot.getValue(User.class));
                }
                invalidateSearch(users);
                showUsersOnMap(users);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //handle error
            }
        });
    }

    private void showUsersOnMap(List<User> users) {
        for (User user : users) {
            showUserOnMap(user);
        }
    }

    private HashMap<User, Marker> usersToMarkers = new HashMap<>();
    private HashMap<Marker, User> markersToUsers = new HashMap<>();

    private void showUserOnMap(User user) {
        MarkerOptions markerOptions = new MarkerOptions()
                .position(new LatLng(user.getLatitude(), user.getLongitude()))
                .icon(notActiveIcon)
                .title(user.getFirstName() + " " + user.getLastName());

        Marker marker = googleMap.addMarker(markerOptions);

        usersToMarkers.put(user, marker);
        markersToUsers.put(marker, user);
    }

    @SuppressWarnings("ResourceType")
    private void findViews() {
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(map);
        gridRecyclerView = (RecyclerView) findViewById(R.id.profile_recycler_grid);
        searchBar = (FloatingSearchView) findViewById(R.id.search_bar);
        mapView = mapFragment.getView();
        plusButton = (ImageView) findViewById(R.id.plus_button);
        recyclerView = (RecyclerView) findViewById(R.id.profile_recycler);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
    }

    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    private static class StringSearchSuggestion implements SearchSuggestion {
        public static final Parcelable.Creator<StringSearchSuggestion> CREATOR
                = new Parcelable.Creator<StringSearchSuggestion>() {
            public StringSearchSuggestion createFromParcel(Parcel in) {
                return new StringSearchSuggestion(in);
            }

            public StringSearchSuggestion[] newArray(int size) {
                return new StringSearchSuggestion[size];
            }
        };
        private final String string;

        private StringSearchSuggestion(String string) {
            this.string = string;
        }

        public StringSearchSuggestion(Parcel in) {
            this.string = in.readString();
        }

        @Override
        public String getBody() {
            return string;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(getBody());
        }
    }
}
