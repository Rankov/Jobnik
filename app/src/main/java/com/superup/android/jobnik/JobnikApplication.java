package com.superup.android.jobnik;

import android.app.Application;
import android.content.Context;

/**
 * Created by vladislavkarpman on 3/9/17.
 */

public class JobnikApplication extends Application {

    private static Context jobnikApplication;


    @Override
    public void onCreate() {
        super.onCreate();
        jobnikApplication = this;
    }

    public static Context getApplication() {
        return jobnikApplication;
    }
}
