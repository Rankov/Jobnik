package com.superup.android.jobnik.model;

/**
 * Created by Andrey Rankov on 09/03/2017.
 */

public class User {

    /**
     * User ID
     */
    public int id;

    /**
     * Url of user's picture
     */
    public String userPic;

    /**
     * First name of the user
     */
    public String firstName;

    /**
     * Last name of the user
     */
    public String lastName;

    /**
     * User's hourly rate
     */
    public double cost;

    /**
     * User's average rating
     */
    public double rating;

    /**
     * User's location latitude
     */
    public double longitude;

    /**
     * User's location longitude
     */
    public double latitude;

    /**
     * User's business area
     */
    public String profession;

    public double getDistanse() {
        return distanse;
    }

    public void setDistanse(double distanse) {
        this.distanse = distanse;
    }

    public double distanse;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserPic() {
        return userPic;
    }

    public void setUserPic(String userPic) {
        this.userPic = userPic;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getProfession() {
        return profession != null ? profession : "abcde";
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }
}
